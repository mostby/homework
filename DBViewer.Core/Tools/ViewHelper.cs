﻿namespace DBViewer
{
    public static class ViewHelper
    {
        private const string ColumnDivider = "         ";

        public static IEnumerable<string> GetTableView<T>(List<T> data) where T : class
        {
            var visibleColumns = new List<string>();
            var view = new List<string>();
            var columnNames = typeof(T).GetProperties()
                        .Select(property => property.Name)
                        .ToArray();            
            foreach ( var name in columnNames )
            {
                if (IsVisibleColumn<T>(name))
                {
                    visibleColumns.Add(name);
                }
            }

            VisualizeTableHeader(view, visibleColumns, typeof(T).Name);

            foreach (var row in data)
            {
                var rowData = new List<string>();
                foreach (var propertyName in visibleColumns)
                {
                    var columnWidth = $"{propertyName}{ColumnDivider}".Length;
                    var value = row
                        .GetType()
                        ?.GetProperty(propertyName)
                        ?.GetValue(row, null);
                        
                    rowData.Add(value?.ToString()?.Trim() ?? string.Empty);
                }
                VisualizeTableRow(rowData, view, visibleColumns);               
            }
            VisualizeTableBottom(view);
            return view;
        }

        /// <summary>
        /// Проверка виртуальных свойств(связей) в классах сущностей таблиц.
        /// В представление, отображение этих полей, входить не должно.
        /// </summary>
        private static bool IsVisibleColumn<T>(string propertyName)
        {
            var obj = (T)Activator.CreateInstance(typeof(T));
            var property = obj?.GetType()?.GetProperty(propertyName);
            if (property != null &&
                (property.PropertyType.IsValueType || property.PropertyType.Equals(typeof(string))))
            {
                return true;
            }
            return false;
        }

        private static void VisualizeTableHeader(List<string> view, List<string> visibleColumns, string tableName)
        {
            view.Add($"Table: {tableName}");
            var headerTop = "╔";
            var headerColumnsName = "║";
            var headerBottom = "╠";

            foreach (var name in visibleColumns)
            {
                headerTop += "".PadLeft(name.Length + ColumnDivider.Length, '═') + "╦";
                headerColumnsName += $"{name}{ColumnDivider}" + "║";
                headerBottom += "".PadLeft(name.Length + ColumnDivider.Length, '═') + "╬";
            }

            view.Add(headerTop.Remove(headerTop.Length - 1) + "╗");
            view.Add(headerColumnsName);
            view.Add(headerBottom.Remove(headerTop.Length - 1) + "╣");
        }

        private static void VisualizeTableRow(List<string> data, List<string> view,
            List<string> visibleColumns)
        {
            var rowData = "║";
            var rowBottom = view[2];
            var index = 0;
            foreach (var cellValue in data)
            {
                var freeSpace = visibleColumns[index].Length + ColumnDivider.Length;
                var value = cellValue;
                if (cellValue.Length > freeSpace)
                {
                    value = cellValue.Substring(0, freeSpace - 2) + ">>";
                }

                rowData += value
                    .PadRight(freeSpace, ' ') + "║";
                ++index;
            }
            view.Add(rowData);          
        }

        private static void VisualizeTableBottom(List<string> view)
        {
            var tableBottom = view[3]
                .Replace("╬", "╩")
                .Replace("╣","╝")
                .Replace("╠", "╚");
            view.Add (tableBottom);
        }
    }
}
