﻿namespace DBViewer
{
    public static class ConsoleEx
    {
        public static void WriteLineCollection(IEnumerable<string> collection)
        { 
            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }
        }
    }
}
