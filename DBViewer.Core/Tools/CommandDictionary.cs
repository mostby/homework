﻿using System.Collections.ObjectModel;

namespace DBViewer
{
    public static class CommandDictionary
    {
        public static IReadOnlyDictionary<string, CommandTypes> Commands = new Dictionary<string, CommandTypes>()
        {
            {@"\ts",CommandTypes.ShowTables},
            {@"\add", CommandTypes.AddDataToTable }
        };
    }
}
