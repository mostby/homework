﻿namespace DBViewer;

public partial class Action
{
    public int ActionId { get; set; }

    public string? Name { get; set; }

    public virtual ICollection<Object> Objects { get; set; } = new List<Object>();
}
