﻿namespace DBViewer;

public partial class Object
{
    public int ObjectId { get; set; }

    public string? Name { get; set; }

    public int? ActionId { get; set; }

    public int? ContractorId { get; set; }

    public decimal? Price { get; set; }

    public virtual Action? Action { get; set; }

    public virtual Contractor? Contractor { get; set; }
}
