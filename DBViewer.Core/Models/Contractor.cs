﻿namespace DBViewer;

public partial class Contractor
{
    public int ContractorId { get; set; }

    public string? Surname { get; set; }

    public string? Name { get; set; }

    public string? SecondName { get; set; }

    public string? Phone { get; set; }

    public string? Email { get; set; }

    public virtual ICollection<Object> Objects { get; set; } = new List<Object>();
}
