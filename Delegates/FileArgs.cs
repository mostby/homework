﻿using System;

namespace Delegates
{
    public class FileArgs : EventArgs
    {
        public string FoundFile { get; }

        public FileArgs(string fileName) => FoundFile = fileName;
    }
}
