﻿using System;
using System.Threading;

namespace Delegates
{
    public class Program
    {
        private static FileSeeker fileSeeker = new FileSeeker();

        static void Main(string[] args)
        {
            var cts = new CancellationTokenSource();
          
            var maxValue = Math.SimpleCollection.GetMax<object>(a => Math.ConvertToNumber(a));
            Console.WriteLine($"Поиск максимального элемента коллекции: {maxValue}");
            
            Console.WriteLine($"Поиск файлов в папке пользователя, для старата нажмите любую клавишу.");
            Console.ReadKey();
            fileSeeker.FileFound += FileSeekerFileFound;
           
            var searchThread = new Thread(fileSeeker.StartSerach);
            searchThread.Start();
            Console.ReadKey();
            searchThread.Abort();
            fileSeeker.FileFound -= FileSeekerFileFound;
        }

        private static void FileSeekerFileFound(object sender, FileArgs e)
        {
            Console.Clear();
            Console.WriteLine($"Найден файл: {e.FoundFile}");
            Console.WriteLine($"Всего {fileSeeker.FilesCount}");
            Console.WriteLine($"Для отмены нажмите любую клавишу.");
        }
    }
}
