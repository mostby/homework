﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Delegates
{
    public static class Math
    {
        public static IEnumerable<Object> SimpleCollection = new List<Object>
        {
            "Some text",
            (double) 100,
            (float) 10,
            (int) 66
        };

        public static object GetMax<T>(this IEnumerable<T> collection, Func<T, float> convertToNumber) where T : class
        {
            var convertCollection = new List<float>(collection.Count());
            foreach (var item in collection)
            {
                convertCollection.Add(convertToNumber(item));
            }
            return convertCollection.Max();
        }

        public static float ConvertToNumber(object a)
        {
            try
            {
                Convert.ToSingle(a);
            }
            catch
            {
                return float.MinValue;
            }
            return Convert.ToSingle(a);
        }
    }
}
