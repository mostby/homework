﻿using System;
using System.IO;
using System.Threading;

namespace Delegates
{
    public class FileSeeker
    {
        private string targetDirectory;
        public event EventHandler<FileArgs> FileFound;
        public int FilesCount;

        public FileSeeker()
        {
            this.targetDirectory = System.IO.Path.GetTempPath();
        }

        public void StartSerach()
        {
            foreach (var file in Directory.EnumerateFiles(targetDirectory))
            {
                ++FilesCount;
                RaiseFileFound(file);
                Thread.Sleep(1000);
            }
        }

        private void RaiseFileFound(string file)
        {
            FileFound?.Invoke(this, new FileArgs(file));
        }      
    }
}
