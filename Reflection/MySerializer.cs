﻿namespace Reflection
{
    public class MySerializer
    {
        public string SerializeObject(object obj)
        {
            var result = string.Empty;
            var fields = obj.GetType().GetFields();
            foreach (var field in fields)
            {
                var value = field.GetValue(obj);
                result += $"{field.Name}={value};";
            }
            return result;
        }

        public T DeserializeObject<T>(string objString) where T : new()
        { 
            var obj = new T();
            var blocks = objString.Split(';');
            foreach (var block in blocks)
            {
                if (string.IsNullOrEmpty(block))
                {
                    continue;
                }
                var fieldDesc = block.Split('=');
                var fieldName = fieldDesc[0];
                var value = fieldDesc[1];   

                var field = obj.GetType().GetField(fieldName);
                if (field != null)
                {
                    dynamic nativeValue = Convert.ChangeType(value, field.FieldType);
                    field.SetValue(obj, nativeValue);
                }
            }
            return obj;
        }
    }
}
