﻿using Newtonsoft.Json;
using Reflection;

var iterationsCount = 1000;
var curentDate = DateTime.Now;
var f = new F { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
var mySerializer = new MySerializer();
var serializeResult = new string[iterationsCount];

Console.WriteLine("Сериализуемый класс: class F { int i1, i2, i3, i4, i5;}");
Console.WriteLine("Код сериализации-десериализации: " +
    "https://gitlab.com/mostby/homework/-/blob/main/Reflection/MySerializerer.cs?ref_type=heads");
Console.WriteLine($"Количество замеров: {iterationsCount} итераций");
Console.WriteLine("Мой рефлекшен:");

for (int i = 0; i < iterationsCount; i++)
{    
    serializeResult[i] = mySerializer.SerializeObject(f);
}
var workTime = (DateTime.Now - curentDate).TotalMilliseconds;
Console.WriteLine($"Время на сеарилизацию = {workTime} мс");

curentDate = DateTime.Now;
for (int i = 0; i < iterationsCount; i++)
{
    var obj = mySerializer.DeserializeObject<F>(serializeResult[i]);
}
workTime = (DateTime.Now - curentDate).TotalMilliseconds;
Console.WriteLine($"Время на десериализацию  =  {workTime} мс");

Console.WriteLine("Стандартный механизм (NewtonsoftJson):");
curentDate = DateTime.Now;
for (int i = 0;i < iterationsCount; i++)
{
    serializeResult[i] = JsonConvert.SerializeObject(f);
}
workTime = (DateTime.Now - curentDate).TotalMilliseconds;
Console.WriteLine($"Время на сериализацию = {workTime} мс");

curentDate = DateTime.Now;
for (int i = 0; i < iterationsCount; i++)
{
    var obj = JsonConvert.DeserializeObject<F>(serializeResult[i]);
}
workTime = (DateTime.Now - curentDate).TotalMilliseconds;
Console.WriteLine($"Время на десериализацию  = {workTime} мс");

Console.ReadLine();