﻿using System;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        static Task Main(string[] args)
        {
            var commandProcessor = new CommandProccesor();
            commandProcessor.Run();
            return null;
        }
    }
}