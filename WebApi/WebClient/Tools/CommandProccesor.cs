﻿using System;

namespace WebClient
{
    public class CommandProccesor
    {
        private WebApiClient webApiClient;

        public CommandProccesor() 
        {
            webApiClient = new WebApiClient();
        }

        public void Run()
        {
            while (true)
            {
                ShowInfo();
                var input = Console.ReadLine();
                ExecuteCommand(input);
            }
            
        }

        private void ExecuteCommand(string input)
        {
            switch (input)
            {
                case "1":
                    Console.WriteLine("Введите ID пользователя: ");
                    var id = Console.ReadLine();
                    var custumer = webApiClient.GetCustumerById(id);
                    Console.WriteLine(custumer);
                    break;
                case "2":
                    var newCustumer = CustomerGenerator.GetRandomCustomer();
                    var message = webApiClient.SendData(newCustumer);
                    Console.WriteLine(message);
                    break;
            }
        }

        private void ShowInfo()
        {
            Console.WriteLine();
            Console.WriteLine("====================================");
            Console.WriteLine("Доступные команды");
            Console.WriteLine("1 Запросить данные клиента по ID");
            Console.WriteLine("2 Добавить случайного пользователя");
            Console.WriteLine("Введите номер команды");
        }
    }
}
