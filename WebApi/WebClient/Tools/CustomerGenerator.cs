﻿using System;
using System.Collections.Generic;

namespace WebClient
{
    public static class CustomerGenerator
    {
        private static readonly Dictionary<int, string> fierstName = new Dictionary<int, string>
        {
            {1, "Андрей" },
            {2, "Михаил" },
            {3, "Василий" },
            {4, "Олег" },
            {5, "Арсений" }            
        };

        private static readonly Dictionary<int, string> lastName = new Dictionary<int, string>
        {
            {1, "Иванов" },
            {2, "Петров" },
            {3, "Сидоров" },
            {4, "Смирнов" },
            {5, "Столбов" }
        };

        public static Customer GetRandomCustomer()
        {
            
            var rnd = new Random();
            var customer = new Customer
            {
                Id = rnd.Next(1,9999),
                Firstname = fierstName[rnd.Next(1, 5)],
                Lastname = lastName[rnd.Next(1, 5)]
            };
            return customer;
        }
    }
}
