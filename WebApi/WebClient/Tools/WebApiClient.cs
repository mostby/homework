﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace WebClient
{
    public class WebApiClient
    {
        private HttpClient client;
        private UriBuilder uriBuilder;
        private readonly string JsonHeader = "application/json";
        private HttpContentHeaders responseHeaders;
        private HttpStatusCode httpStatus;

        public WebApiClient() 
        {
            client = new HttpClient();
            uriBuilder = new UriBuilder
            {
                Scheme = "http",
                Host = "localhost",
                Port = 5000
            };
        }

        public string GetCustumerById(string id)
        {
            var query = $"customers/{id}";
            var result = GetData(query);
            return result;
        }

        public string AddCustumer(Customer customer)
        {
            var id = SendData(customer);
            return string.Empty;
        }

        public string GetData(string query)
        {
            using (var handler = new HttpClientHandler())
            using (var client = new HttpClient(handler))
            {
                client.DefaultRequestHeaders.Add("Accept", JsonHeader);
                var response = client.GetAsync(uriBuilder.Uri + query).Result;
                responseHeaders = response.Content.Headers;
                httpStatus = response.StatusCode;   
                var responseBody = response.Content.ReadAsStreamAsync().Result;
                var reader = new StreamReader(responseBody);
                var httpResult = reader.ReadToEnd();
                return httpResult;
            }
        }

        public string SendData(Customer customer)
        {
            using (var handler = new HttpClientHandler())
            using (var client = new HttpClient(handler))
            {
                client.DefaultRequestHeaders.Add("Accept", JsonHeader);
                var content = new StringContent(JsonConvert.SerializeObject(customer),
                    Encoding.UTF8, "application/json");
                var response = client.PostAsync(uriBuilder.Uri + "customers", content).Result;
                responseHeaders = response.Content.Headers;
                httpStatus = response.StatusCode;
                var responseBody = response.Content.ReadAsStreamAsync().Result;
                var reader = new StreamReader(responseBody);
                var httpResult = reader.ReadToEnd();
                return httpResult;
            }
        }
    }
}
