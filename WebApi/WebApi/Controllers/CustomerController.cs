using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        DataService dataService;

        public CustomerController (DataService dataService)
        {
            this.dataService = dataService; 
        }

        [HttpGet("{id:long}")]   
        public ObjectResult GetCustomerAsync([FromRoute] long id)
        {
            var custumer = dataService.customers.FirstOrDefault(x => x.Id == id);
            if (custumer == null)
            {
                return NotFound("������������ �� ������");
            }
            return Ok(custumer);
        }

        [HttpPost("")]   
        public ObjectResult CreateCustomerAsync([FromBody] Customer customer)
        {
            if (dataService.customers.FirstOrDefault(x => x.Id == customer.Id) != null)
            {
                Conflict($"������������ � Id = {customer.Id} ��� ����������");
            }
            dataService.customers.Add(customer);
            return Ok($"������������ {customer.Firstname} {customer.Lastname}, Id = {customer.Id} ��������");
        }
    }
}