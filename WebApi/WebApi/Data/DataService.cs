﻿using System.Collections.Generic;
using WebApi.Models;

namespace WebApi
{
    public class DataService
    {
        public List<Customer> customers = new List<Customer>
        {
            new Customer {Id = 1, Firstname = "Иван", Lastname = "Иванов"},
            new Customer {Id = 2, Firstname = "Андрей", Lastname = "Новиков"}
        };                
    }
}
