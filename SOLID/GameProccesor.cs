﻿namespace SOLID
{
    public class GameProccesor
    {
        private IGame game;
        private BaseConsole baseConsole;
        private BaseConsole gameConsole;

        public GameProccesor(IGame game)
        {
            this.game = game;
            this.baseConsole = new BaseConsole();
            this.gameConsole = new GameConsole();            
        }

        public void Run()
        {
            gameConsole.ShowMessage("Пожалуйста введите число:");
            while (true)
            {
                var input = Console.ReadLine();
                baseConsole.ShowMessage(game.Play(input));
            }
        }
    }
}
