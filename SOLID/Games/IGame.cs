﻿namespace SOLID
{
    public interface IGame
    {
        string Play(string? userInput);
    }
}
