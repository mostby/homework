﻿namespace SOLID
{
    public class NumbersGame : IGame
    {
        private int num;

        public NumbersGame()
        {
            num = GenerateNumber();
        }

        public string Play(string? userInput)
        {
            if (Convert.ToInt32(userInput) < num)
            {
                return "Число меньше загаданного";
            }
            if (Convert.ToInt32(userInput) > num)
            {
                return "Число больше загаданного";
            }
            if (Convert.ToInt32(userInput) == num)
            {
                return "Верно!";
            }
            throw new Exception();
        }

        private int GenerateNumber()
        {
            var rnd = new Random();
            return rnd.Next(Convert.ToInt32(Settings.RangeMin), Convert.ToInt32(Settings.RangeMax));
        }
    }
}
