﻿namespace SOLID
{
    public class BaseConsole: IConsole
    {
        public virtual void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
