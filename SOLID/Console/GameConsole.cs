﻿namespace SOLID
{
    public class GameConsole : BaseConsole
    {
        public override void ShowMessage(string message)
        {
            Console.WriteLine("===========================================");
            Console.WriteLine(message);
            Console.WriteLine("===========================================");
        }
    }
}
