﻿namespace SOLID
{
    public interface IConsole
    {
        void ShowMessage(string message);
    }
}
