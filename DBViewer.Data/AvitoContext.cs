﻿using Microsoft.EntityFrameworkCore;

namespace DBViewer;

public partial class AvitoContext : DbContext
{
    public AvitoContext()
    {
        Database.EnsureCreated();
    }

    public AvitoContext(DbContextOptions<AvitoContext> options)
        : base(options)
    {
    }
    public static string ConnectionString = string.Empty;

    public virtual DbSet<Action> Actions { get; set; }

    public virtual DbSet<Contractor> Contractors { get; set; }

    public virtual DbSet<Object> Objects { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseNpgsql(ConnectionString, options => options.SetPostgresVersion(new Version(9, 5)));

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.HasSequence<int>("Actions_ActionId_seq").StartsAt(3L);
        modelBuilder.HasSequence<int>("Contractors_ContractorId_seq").StartsAt(2L);
        modelBuilder.HasSequence<int>("Objects_ObjectId_seq").StartsAt(2L);

        modelBuilder.Entity<Action>(entity =>
        {
            entity.HasKey(e => e.ActionId).HasName("PK_Action");

            entity.Property(e => e.ActionId)
                .HasDefaultValueSql("nextval('\"Actions_ActionId_seq\"')");
            
            entity.Property(e => e.Name)
                .HasMaxLength(100)
                .IsFixedLength();
        });

        modelBuilder.Entity<Contractor>(entity =>
        {
            entity.HasKey(e => e.ContractorId).HasName("PK_Contractor");

            entity.Property(e => e.ContractorId)
                 .HasDefaultValueSql("nextval('\"Contractors_ContractorId_seq\"')");
            entity.Property(e => e.Email)
                .HasMaxLength(100)
                .IsFixedLength();
            entity.Property(e => e.Name)
                .HasMaxLength(100)
                .IsFixedLength();
            entity.Property(e => e.Phone)
                .HasMaxLength(100)
                .IsFixedLength();
            entity.Property(e => e.SecondName)
                .HasMaxLength(100)
                .IsFixedLength();
            entity.Property(e => e.Surname)
                .HasMaxLength(100)
                .IsFixedLength();
        });

        modelBuilder.Entity<Object>(entity =>
        {
            entity.HasKey(e => e.ObjectId).HasName("PK_Object");

            entity.Property(e => e.ObjectId)
                .HasDefaultValueSql("nextval('\"Objects_ObjectId_seq\"')");

            entity.Property(e => e.Name)
                .HasMaxLength(100)
                .IsFixedLength();

            entity.HasOne(d => d.Action).WithMany(p => p.Objects)
                .HasForeignKey(d => d.ActionId)
                .HasConstraintName("FK_ActionId_Object_To_Action");

            entity.HasOne(d => d.Contractor).WithMany(p => p.Objects)
                .HasForeignKey(d => d.ContractorId)
                .HasConstraintName("FK_ContractorId_Object_To_Contractor");
        });

        modelBuilder.Entity<Contractor>().HasData(
            new Contractor[]
            {
                new Contractor
                {
                    ContractorId = 1,
                    Email = "yyy@yyy.ru",
                    Name = "Иван",
                    Surname = "Иванов",
                    SecondName = "Иванович"
                }
            });

        modelBuilder.Entity<Action>().HasData(
            new Action[]
            {
                new Action { ActionId = 1, Name = "Покупка" },
                new Action { ActionId = 2, Name = "Продажа"}
            });

        modelBuilder.Entity<Object>().HasData(
            new Object[]
            {
                new Object {ObjectId = 1, ActionId = 1, ContractorId = 1, Price = 199, Name = "Мотоцикл"}
            });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
