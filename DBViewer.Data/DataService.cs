﻿namespace DBViewer
{
    public class DataService
    {
        public void AddData<T>(T entity) where T : class
        {
            using (var db = new AvitoContext())
            {
                db.Set<T>().Add(entity);
                db.SaveChanges();
            }
        }

        public IEnumerable<string> GetTableView<T>() where T : class
        {
            using (var db = new AvitoContext())
            {
                var data = db.Set<T>().ToList();
                var view = ViewHelper.GetTableView(data);
                
                return view;
            }

        }
    }
}