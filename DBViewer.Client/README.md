﻿Для запуска приложения необходимо добавить параметр командной строки. 
В виде строки подключения к базе pg, без ключа.
"Host=localhost;Port=5432;Database=Avito;Username=postgres;Password=qwe123"

Внимание !!!
Используется старая СУБД postgres 9.5
Если подключаемся к новым версиям убираем опцию в контексте.
options => options.SetPostgresVersion(new Version(9, 5))

