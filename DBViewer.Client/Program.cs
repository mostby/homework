﻿using DBViewer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

HostApplicationBuilder builder = Host.CreateApplicationBuilder(args);
builder.Logging.ClearProviders();
builder.Services.AddSingleton<DataService>();
builder.Services.AddSingleton<MainService>();
using IHost host = builder.Build();

var listener = host.Services.GetRequiredService<MainService>();

if (args.Count() == 0)
{
    throw new ArgumentException("Укажите строку подключения к базе в параметрах командной строки. README.md");
}
AvitoContext.ConnectionString = args[0];

listener.Run();
await host.RunAsync();




