﻿namespace DBViewer
{
    public class MainService
    {
        private readonly DataService dataService;

        public MainService(DataService dataService) 
        { 
            this.dataService = dataService;
            
        }

        public void  Run()
        {           
            new Thread(() =>
            {
                while (true)
                {
                    ShowInfo();
                    var input = Console.ReadLine();
                    RunCommand(input);
                }
            }).Start();
        }

        private void RunCommand(string? input)
        {
            Console.Clear();
            var args = input?.Trim().Split(" ") ?? null;
            
            if (args == null || args.Count() == 0) 
            {
                ShowInfo();
                return;
            }

            var command = args[0];
            var tables = args.Count() > 1 ? args[1] : string.Empty;
            var values = args.Count() > 2 ? args[2] : string.Empty;

            if (CommandDictionary.Commands.ContainsKey(command))
            {
                ExecuteCommand(CommandDictionary.Commands[command], tables, values);
            }
        }

        private void ExecuteCommand(CommandTypes commandTypes, string table, string values)
        {
            switch (commandTypes)
            {
                case CommandTypes.ShowTables:
                    ShowTables();
                    break;
                case CommandTypes.AddDataToTable:
                    AddDataToTable(table, values);
                    break;
            }
        }

        private void ShowInfo()
        {
            Console.WriteLine("Параметры командной строки программы");
            Console.WriteLine("\\ts - Отобразить содержимое всех таблиц");
            Console.WriteLine("\\add [Имя таблицы] [Значение1,Значение2,...] - Добавить данные в таблицу");
        }

        private void AddDataToTable(string tableName, string values)
        {
            if (string.IsNullOrEmpty(tableName) || string.IsNullOrEmpty(values))
            {
                ShowInfo();
            }
            var rowValues = values.Split(",");
            switch (tableName.ToLower())
            {
                case "object":
                    AddRowToObject(rowValues);
                    break;
                case "action":
                    AddRowToAction(rowValues);
                    break;
                case "contractor":
                    AddRowToContractor(rowValues);
                    break;
                default :
                    Console.WriteLine("<WARNING> допустимые имена таблиц: Action, Contractor, Object");
                    break;
            }
        }

        private void AddRowToContractor(string[] rowValues)
        {
            try
            {
                var entity = new Contractor
                {
                    Surname = rowValues[0],
                    Name = rowValues[1],
                    SecondName = rowValues[2],
                    Phone = rowValues[3],
                    Email = rowValues[4],
                };
                dataService.AddData<Contractor>(entity);
            }
            catch (Exception ex)
            {                
                Console.WriteLine(ex.Message);
                Console.WriteLine("Пример >add Contractor Surname,Name,SecondName,Phone,Email");
                ShowInfo();
            }
        }

        private void AddRowToAction(string[] rowValues)
        {
            try
            {
                var entity = new Action
                {                   
                    Name = rowValues[0],
                };
                dataService.AddData<Action>(entity);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Пример >add Action name");
                ShowInfo();
            }
        }

        private void AddRowToObject(string[] rowValues)
        {
            try
            {
                var entity = new Object
                {
                    Name = rowValues[0],
                    ActionId = Convert.ToInt32(rowValues[1]),
                    ContractorId = Convert.ToInt32(rowValues[2]),
                    Price = Convert.ToDecimal(rowValues[3])
                };
                dataService.AddData<Object>(entity);
            }
            catch(Exception ex)
            {                
                Console.WriteLine(ex.Message);
                Console.WriteLine("Пример >add Object Велосипед,1,1,200");
                ShowInfo();
            }
        }

        private void ShowTables()
        {
            ConsoleEx.WriteLineCollection(dataService.GetTableView<Object>());
            ConsoleEx.WriteLineCollection(dataService.GetTableView<Action>());
            ConsoleEx.WriteLineCollection(dataService.GetTableView<Contractor>());
        }
    }
}
